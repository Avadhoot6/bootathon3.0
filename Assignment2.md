# Data Flow Diagram

DFD provides the functional overview of a system. The graphical representation easily overcomes any gap between ’user and system analyst’ and ‘analyst and system designer’ in understanding a system.

DFD shows the external entities from which data flows into the process and also the other flows of data within a system. It also includes the transformations of data flow by the process and the data stores to read or write a data.

| Term            | Remarks                                            |
| --------------- | -------------------------------------------------- |
| External entity | Name is written inside the rectangle               |
| Process         | Name of the process is written inside the circle   |
| Data store	  | name of the data store is written inside the shape |
| Data flow       | Represented by a directed arc with its data name   |

#  Explanation of Symbols used in DFD

  * **Process** : Processes are represented by circle. The name of the process is written into the circle. The name of the process is usually given in such a way that represents the functionality of the process.
  * **External entity** : External entities are represented by a rectangle and the name of the external entity is written into the shape. These send data to be processed and again receive the processed data.
  * **Data store** : Data stares are represented by a left-right open rectangle. Name of the data store is written in between two horizontal lines of the open rectangle. Data stores are used as repositories from which data can be flown in or flown out to or from a process.
  * **Data flow** : Data flows are shown as a directed edge between two components of a Data Flow Diagram. Data can flow from external entity to process, data store to process, in between two processes and vice-versa.




